package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	wg.Add(2) // горунтыны которые будут вызваны

	// вызываем горутины
	go createItem(rand.Intn(100))
	go createItem(rand.Intn(100)) // по стеку выоплниться раньше

	wg.Wait() // ожидаем завершение групыы грутин
}

func createItem(id int) {
	defer wg.Done()
	fmt.Printf("INSERT (id) VALUES(id = %d)\n", id)
	time.Sleep(2 * time.Second) // условная задержка
	fmt.Printf("NOTIFY: Created item id = %d\n", id)
}
